# Symfony task
## Setup
 ````
 composer install
 
 bin/console doctrine:database:create
 
 bin/console doctrine:schema:create
````

Do not forget to put the correct databse credentials in .env file.

## Data
 Command get-xml-data grabs the XML feed from given (or default) URL and stores it locally
 
 To get the feed 4 times a day, simply set up crontab to execute get-xml-data command every six hours.
 
 Add the following line to crontab (change PATH_TO_PROJECT accordingly)
````
* */6 * * * php PATH_TO_PROJECT/bin/console app:get-xml-feed
````
 
 XML files are saved in /var/downloads/xml directory.
 
 ## API
 
### Read list of existing Products
````
GET /api/products
````

### Read data of a Product 
````
GET /api/products/{id}
````

### Create a new Product
````
POST /api/products
{
    'product_id' => "required",
    'title' => "required",
    'link' => "required",
    'description' => "optional",
    'image_link' => "required",
    'brand' => "required",
    'cond' => "required",
    'availability' => "required",
    'price' => "required",
    'shipping_country' => "required",
    'shipping_price' => "required",
    'shipping_service' => "required",
    'google_product_category' => "required",
}
````
Params:


### Change an existing Product
````
PUT /api/products/{id}
{
    'product_id' => "required",
    'title' => "required",
    'link' => "required",
    'description' => "optional",
    'image_link' => "required",
    'brand' => "required",
    'cond' => "required",
    'availability' => "required",
    'price' => "required",
    'shipping_country' => "required",
    'shipping_price' => "required",
    'shipping_service' => "required",
    'google_product_category' => "required",
}
````

### Remove an existing Product
````
DELETE /api/products/{id}
````

## Logger

Logs are saved after Kernel event OnFitleredResponse is dispatched. Both Request and Response are accessible at this time, 
so it is convenient place to grab them. It's probably not the best place if we consider microtiming and high number of 
requests per second.

Logs are saved to %kernel.logs_dir%/api.log file. One entry per line in order of Request -> Response, with simplified/filtered content (method, uri, content).



