<?php
namespace App\Tests\Command;

use App\Command\GetXMLFeedCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;

class GetXMLFeedCommandTest extends KernelTestCase
{

    public function testCustomFeedURL()
    {
        $input['--url'] = "https://www.w3schools.com/xml/note.xml";

        $this->executeCommand($input);
    }

    public function testDefaultFeedURL()
    {
        $input = [];
        $this->executeCommand($input);
    }

    public function executeCommand(array $arguments, array $inputs = [])
    {
        self::bootKernel();

        $command = new GetXMlFeedCommand(new Filesystem());
        $command->setApplication(new Application(self::$kernel));

        $commandTester = new CommandTester($command);
        $commandTester->setInputs($inputs);
        $commandTester->execute($arguments);

        $output = $commandTester->getDisplay();
        $this->assertContains('Success!', $output);
    }
}
