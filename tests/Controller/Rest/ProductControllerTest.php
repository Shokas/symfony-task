<?php
namespace App\Tests\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    private $http;
    private $baseUri;

    protected function setUp()
    {
        $this->http = new \GuzzleHttp\Client();
        $this->baseUri = 'http://localhost:8000/api/products';
    }

    /**
     * @test
     */
    public function testPostProduct()
    {
        $data = array(
            'product_id' => "test_product",
            'title' => "test_title",
            'link' => "test_link",
            'description' => "test_description",
            'image_link' => "test_image_link",
            'brand' => "test_brand",
            'cond' => "test_cond",
            'availability' => "test_availability",
            'price' => "test_price",
            'shipping_country' => "test_shipping_countr<y",
            'shipping_price' => "test_shipping_price",
            'shipping_service' => "test_shipping_service",
            'google_product_category' => "google_product_category",
        );

        $response = $this->http->request('POST', $this->baseUri, [
            'body' => json_encode($data)
        ]);

        $this->assertEquals(201, $response->getStatusCode());
        $responseBody = json_decode($response->getBody(true), true);
        foreach ($data as $key => $value) {
            $this->assertArrayHasKey($key, $responseBody);
            $this->assertEquals($responseBody[$key], $value);
        }

    }

    public function testBadPostProduct()
    {
        $data = array(
            'product_id' => "test_product",
        );

        $response = $this->http->request('POST', $this->baseUri, [
            'body' => json_encode($data),
            'http_errors' => false
        ]);

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testExistingPostProduct()
    {
        $data = array(
            'product_id' => "test_product",
            'title' => "test_title",
            'link' => "test_link",
            'description' => "test_description",
            'image_link' => "test_image_link",
            'brand' => "test_brand",
            'cond' => "test_cond",
            'availability' => "test_availability",
            'price' => "test_price",
            'shipping_country' => "test_shipping_countr<y",
            'shipping_price' => "test_shipping_price",
            'shipping_service' => "test_shipping_service",
            'google_product_category' => "google_product_category",
        );

        $response = $this->http->request('POST', $this->baseUri, [
            'body' => json_encode($data),
            'http_errors' => false
        ]);

        $this->assertEquals(400, $response->getStatusCode());
        $responseBody = json_decode($response->getBody(true), true);
        $this->assertEquals('Product already exist!', $responseBody);


    }

    public function testGetProducts()
    {
        $response = $this->http->request('GET',  $this->baseUri);
        $this->assertEquals(200, $response->getStatusCode());

    }

    public function testGetProduct()
    {
        $response = $this->http->request('GET',  $this->baseUri . '/test_product');
        $this->assertEquals(200, $response->getStatusCode());

    }

    public function testPutProduct()
    {
        $data = array(
            'product_id' => "test_product_id",
            'title' => "test_title",
            'link' => "test_link",
            'description' => "test_description",
            'image_link' => "test_image_link",
            'brand' => "test_brand",
            'cond' => "test_cond",
            'availability' => "test_availability",
            'price' => "test_price",
            'shipping_country' => "test_shipping_countr<y",
            'shipping_price' => "test_shipping_price",
            'shipping_service' => "test_shipping_service",
            'google_product_category' => "google_product_category",
        );

        $response = $this->http->request('PUT', $this->baseUri . '/test_product', [
            'body' => json_encode($data)
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $responseBody = json_decode($response->getBody(true), true);
        foreach ($data as $key => $value) {
            $this->assertArrayHasKey($key, $responseBody);
            $this->assertEquals($responseBody[$key], $value);
        }

    }

    public function testBadPutProduct()
    {
        $data = array(
            'product_id' => "test_product_id",
            'title' => "test_title",
            'link' => "test_link",
        );

        $response = $this->http->request('PUT', $this->baseUri . '/test_product', [
            'body' => json_encode($data),
            'http_errors' => false
        ]);

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testDeleteProduct()
    {
        $response = $this->http->request('DELETE',  $this->baseUri . '/test_product_id');
        $this->assertEquals(204, $response->getStatusCode());

    }

    public function testConfirmDeletedConfirmNotFoundProduct()
    {
        $response = $this->http->request('GET',  $this->baseUri . '/test_product_id', ['http_errors' => false]);
        $this->assertEquals(404, $response->getStatusCode());

    }

    public function testInexistantPutProduct()
    {
        $data = array(
            'product_id' => "test_product_id",
            'title' => "test_title",
            'link' => "test_link",
            'description' => "test_description",
            'image_link' => "test_image_link",
            'brand' => "test_brand",
            'cond' => "test_cond",
            'availability' => "test_availability",
            'price' => "test_price",
            'shipping_country' => "test_shipping_countr<y",
            'shipping_price' => "test_shipping_price",
            'shipping_service' => "test_shipping_service",
            'google_product_category' => "google_product_category",
        );

        $response = $this->http->request('PUT', $this->baseUri . '/test_product', [
            'body' => json_encode($data),
            'http_errors' => false
        ]);

        $this->assertEquals(404, $response->getStatusCode());
        $responseBody = json_decode($response->getBody(true), true);
        $this->assertEquals('Product does not exist!', $responseBody);
    }

    public function tearDown() {
        $this->http = null;
    }
}