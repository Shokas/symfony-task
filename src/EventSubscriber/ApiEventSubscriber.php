<?php
namespace App\EventSubscriber;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Psr\Log\LoggerInterface;

class ApiEventSubscriber implements EventSubscriberInterface
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        /*
         * Full Info Logs
        $this->logger->info($event->getRequest());

        $this->logger->info($event->getResponse());

         */
        // A bit cleaned up version logs with main info
        $this->logger->info(microtime() . " " . $event->getRequest()->getMethod() . " " . $event->getRequest()->getRequestUri() . " " . $event->getRequest()->getContent());

        $this->logger->info(microtime() . " " . $event->getResponse()->getStatusCode() . " " . $event->getResponse()->getContent());
    }
}