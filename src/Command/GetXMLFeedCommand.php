<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;
use GuzzleHttp;

class GetXMLFeedCommand extends Command
{
    protected static $defaultName = 'app:get-xml-feed';

    private $fileSystem;
    private $directory; //FIXME

    public function __construct(Filesystem $fs)
    {
        parent::__construct();

        $this->fileSystem = $fs;
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $current_dir_path = getcwd();
        $this->directory = $current_dir_path . "/var/downloads/xml";

        if (!$this->fileSystem->exists($this->directory)) {
            try {
                $old = umask(0);
                $this->fileSystem->mkdir($this->directory, 0775);
                umask($old);
            } catch (IOException $exception) {
                throw new RuntimeException(sprintf('Error! Cannot create directory at "%s"', $exception->getPath()));
            }
        }
    }

    protected function configure()
    {
        $this
            ->setDescription('Get XML feed from url and save it locally')
            ->setHelp(<<<'HELP'
The <info>%command.name%</info> command gets the XML feed from given URL:
  <info>php %command.full_name%</info>
By default the command takes feed from https://shop.bite.lt/product_feed/facebook.xml. To set different url, use
 <comment>--url</comment> option:
  <info>php %command.full_name%</info> <comment>--url="https://shop.bite.lt/product_feed/facebook.xml"</comment>
HELP
            )
            ->addOption('url', null, InputOption::VALUE_OPTIONAL, 'The URL of the XML feed', "https://shop.bite.lt/product_feed/facebook.xml");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = $input->getOption('url');

        $client = new GuzzleHttp\Client();

        try {
            $response = $client->request('GET', $url, [
                'headers' => ['Accept' => 'application/xml'],
                'timeout' => 120,
            ])->getBody()->getContents();
        } catch (GuzzleHttp\Exception\GuzzleException $e) {
            throw new RuntimeException(sprintf('Error! Cannot read the feed.'));
        }

        $file = $this->saveFile($response);

        $output->writeln('Success! Feed saved to ' . $file);
    }

    private function saveFile($feed)
    {
        $file = date('YmdHms') . '-feed.xml';
        $filename = $this->directory . '/' . $file;

        try {
            $this->fileSystem->dumpFile($filename, $feed);
        } catch (IOException $exception) {
            throw new RuntimeException(sprintf('Error! Cannot create file in "%s"', $exception->getPath()));
        }

        return $file;
    }
}