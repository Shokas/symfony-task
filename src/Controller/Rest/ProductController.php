<?php

namespace App\Controller\Rest;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class ProductController extends FOSRestController
{
    /**
     * Create Product.
     * @Rest\Post("/products")
     * @param Request $request
     * @return View
     */
    public function postProduct(Request $request ): View
    {
        // TODO DRY validation
        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'product_id' => new Assert\NotBlank(),
            'title' => new Assert\NotBlank(),
            'link' => new Assert\NotBlank(),
            'description' => new Assert\Optional(),
            'image_link' => new Assert\NotBlank(),
            'brand' => new Assert\NotBlank(),
            'cond' => new Assert\NotBlank(),
            'availability' => new Assert\NotBlank(),
            'price' => new Assert\NotBlank(),
            'shipping_country' => new Assert\NotBlank(),
            'shipping_price' => new Assert\NotBlank(),
            'shipping_service' => new Assert\NotBlank(),
            'google_product_category' => new Assert\NotBlank(),
        ));

        $violations = $validator->validate($request->request->all(), $constraint);

        if (count($violations) > 0) {
            return View::create($violations, Response::HTTP_BAD_REQUEST);
        }

        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneBy(
                ['product_id' => $request->get('product_id')]
            );

        if ($product) {
            return View::create('Product already exist!', Response::HTTP_BAD_REQUEST);
        }

        $product = new Product();
        $product->setProductId($request->get('product_id'));
        $product->setTitle($request->get('title'));
        $product->setDescription($request->get('description'));
        $product->setLink($request->get('link'));
        $product->setImageLink($request->get('image_link'));
        $product->setBrand($request->get('brand'));
        $product->setCond($request->get('cond'));
        $product->setAvailability($request->get('availability'));
        $product->setPrice($request->get('price'));
        $product->setShippingCountry($request->get('shipping_country'));
        $product->setShippingService($request->get('shipping_service'));
        $product->setShippingPrice($request->get('shipping_price'));
        $product->setGoogleProductCategory($request->get('google_product_category'));

        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->persist($product);
        $entityManager->flush();

        return View::create($product, Response::HTTP_CREATED);
    }

    /**
     * Retrieves a Product resource
     * @Rest\Get("/products/{productId}")
     * @ParamConverter("product", options={"mapping": {"productId": "product_id"}})
     */
    public function getProduct(Product $product = null): View
    {
        if(!$product)
            return View::create('Product does not exist!', Response::HTTP_NOT_FOUND);

        return View::create($product, Response::HTTP_OK);
    }

    /**
     * Retrieves a collection of Products resource
     * @Rest\Get("/products")
     */
    public function getProducts(): View
    {
        $products = $this->getDoctrine()
                ->getRepository(Product::class)
                ->findAll();

        return View::create($products, Response::HTTP_OK);
    }

    /**
     * Replaces Product resource
     * @Rest\Put("/products/{productId}")
     * @ParamConverter("product", options={"mapping": {"productId": "product_id"}})
     */
    public function putProduct(Request $request, Product $product = null): View
    {
        // TODO DRY validation
        $validator = Validation::createValidator();

        $constraint = new Assert\Collection(array(
            'product_id' => new Assert\NotBlank(),
            'title' => new Assert\NotBlank(),
            'link' => new Assert\NotBlank(),
            'description' => new Assert\Optional(),
            'image_link' => new Assert\NotBlank(),
            'brand' => new Assert\NotBlank(),
            'cond' => new Assert\NotBlank(),
            'availability' => new Assert\NotBlank(),
            'price' => new Assert\NotBlank(),
            'shipping_country' => new Assert\NotBlank(),
            'shipping_price' => new Assert\NotBlank(),
            'shipping_service' => new Assert\NotBlank(),
            'google_product_category' => new Assert\NotBlank(),
        ));

        $violations = $validator->validate($request->request->all(), $constraint);

        if (count($violations) > 0) {
            return View::create($violations, Response::HTTP_BAD_REQUEST);
        }

        if ($product) {
            $product->setProductId($request->get('product_id'));
            $product->setTitle($request->get('title'));
            $product->setDescription($request->get('description') ? $request->get('description') : $product->getDescription());
            $product->setLink($request->get('link'));
            $product->setImageLink($request->get('image_link'));
            $product->setBrand($request->get('brand'));
            $product->setCond($request->get('cond'));
            $product->setAvailability($request->get('availability'));
            $product->setPrice($request->get('price'));
            $product->setShippingCountry($request->get('shipping_country'));
            $product->setShippingService($request->get('shipping_service'));
            $product->setShippingPrice($request->get('shipping_price'));
            $product->setGoogleProductCategory($request->get('google_product_category'));

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($product);
            $entityManager->flush();
        } else {
            return View::create('Product does not exist!', Response::HTTP_NOT_FOUND);
        }

        return View::create($product, Response::HTTP_OK);
    }


    /**
     * Removes the Product resource
     * @Rest\Delete("/products/{productId}")
     * @ParamConverter("product", options={"mapping": {"productId": "product_id"}})
     */
    public function deleteArticle(Product $product = null): View
    {
        if ($product) {
            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->remove($product);
            $entityManager->flush();
        } else {
            return View::create('Product does not exist!', Response::HTTP_NOT_FOUND);
        }

        return View::create([], Response::HTTP_NO_CONTENT);
    }
}
